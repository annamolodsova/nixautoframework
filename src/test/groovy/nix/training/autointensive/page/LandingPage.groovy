package nix.training.autointensive.page

import geb.Page

class LandingPage extends Page {

    static content = {
        productsLink (wait: true) { $("a", text: "Products") }
    }
    static at = {
        title == "NIX – Outsourcing Offshore Software Development Company"
    }

    def "User navigates to product page"(){
        productsLink.click()
    }
}
