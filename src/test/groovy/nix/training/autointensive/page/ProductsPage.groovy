package nix.training.autointensive.page

import geb.Page

class ProductsPage extends Page {

    static at = {
        title == "NIX Products for Mobile & Desktop | NIX"
    }
}
